﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="WebApp.Main" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2>Menu principal</h2>
        <hr />
        <br />
        <div>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Listar Todos os Livros com Seus Conteúdos" OnClick="BtnListarLivros"></asp:Button>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Listar Apenas Autores" OnClick="BtnListarAutores"></asp:Button>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Listar Apenas Titulos" OnClick="BtnListarTitulos"></asp:Button>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Listar Temas" OnClick="BtnListarTemas"></asp:Button>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Pesquisar Livros por Temas" OnClick="BtnPesquisarLivrosTema"></asp:Button>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Pesquisar Livros por Autor" OnClick="BtnPesquisarLivrosAutor"></asp:Button>
        </div>
    </div>
</asp:Content>
