﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class LivrosAutor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int idAutor = Convert.ToInt32(Session["Autor"]);
            var livros = new LivrariaService().ObterLivrosPorAutor(idAutor);
            string autor = new LivrariaService().ObterAutorPorId(idAutor).Nome;

            HeaderLivros.InnerText += " de " + autor;

            if (livros != null && livros.Count > 0)
            {
                this.GridLivros.DataSource = livros;
                this.GridLivros.DataBind();
            }
        }

        protected void VoltarBtn(object sender, EventArgs e)
        {
            Response.Redirect("PesquisaAutor.aspx");
        }
    }
}