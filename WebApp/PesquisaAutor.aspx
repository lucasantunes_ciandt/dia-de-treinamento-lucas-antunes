﻿<%@ Page Title="Pesquisa Autor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PesquisaAutor.aspx.cs" Inherits="WebApp.PesquisaAutor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2>Insira o Id do Autor desejado</h2>
        <hr />
        <br />
        <asp:TextBox id="IdAutor" class="custom-text-box" runat="server" Height="23px" BorderStyle="Inset"></asp:TextBox>
        <asp:Button class="custom-btn" runat="server" Text="Pesquisar" OnClick="PesquisarBtn"/>
     </div>
</asp:Content>
