﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Temas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var temas = new LivrariaService().ObterTemas();

                if (temas != null && temas.Count > 0)
                {
                    this.GridLivros.DataSource = temas;
                    this.GridLivros.DataBind();
                }
            }
        }
    }
}