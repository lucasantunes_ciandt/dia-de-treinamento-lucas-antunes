﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnListarLivros(object sender, EventArgs e)
        {
            Response.Redirect("Livros.aspx");
        }
        protected void BtnListarAutores(object sender, EventArgs e)
        {
            Response.Redirect("Autores.aspx");
        }
        protected void BtnListarTitulos(object sender, EventArgs e)
        {
            Response.Redirect("Titulos.aspx");
        }
        protected void BtnListarTemas(object sender, EventArgs e)
        {
            Response.Redirect("Temas.aspx");
        }
        protected void BtnPesquisarLivrosTema(object sender, EventArgs e)
        {
            Response.Redirect("PesquisaTema.aspx");
        }
        protected void BtnPesquisarLivrosAutor(object sender, EventArgs e)
        {
            Response.Redirect("PesquisaAutor.aspx");
        }
    }
}