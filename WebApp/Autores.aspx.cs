﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Service;
using Infrastructure;

namespace WebApp
{
    public partial class Autores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var autores = new LivrariaService().ObterAutores();

                if (autores!= null && autores.Count > 0)
                {
                    this.GridLivros.DataSource = autores;
                    this.GridLivros.DataBind();
                }
            }
        }
    }
}