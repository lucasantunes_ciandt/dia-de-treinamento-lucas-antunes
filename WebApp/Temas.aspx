﻿<%@ Page Title="Temas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Temas.aspx.cs" Inherits="WebApp.Temas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2>Temas</h2>
        <hr />
        <br />
        <div>
            <asp:GridView id="GridLivros" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" />
                    <asp:BoundField DataField="Descricao" HeaderText="Descrição"/>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
