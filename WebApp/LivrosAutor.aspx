﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LivrosAutor.aspx.cs" Inherits="WebApp.LivrosAutor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2 id="HeaderLivros" runat="server">Livros</h2>
        <hr />
        <br />
        <div>
            <asp:GridView id="GridLivros" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id"/>
                    <asp:BoundField DataField="Titulo" HeaderText="Título"/>
                    <asp:BoundField DataField="Valor" HeaderText="Valor" DataFormatString="{0:C}"/>
                    <asp:BoundField DataField="IdAutor" HeaderText="IdAutor"/>
                    <asp:BoundField DataField="IdTema" HeaderText="IdTema" />
                </Columns>
            </asp:GridView>
        </div>
        <hr />
        <asp:Button class="custom-btn" runat="server" Text="Voltar" OnClick="VoltarBtn"/>
    </div>
</asp:Content>
