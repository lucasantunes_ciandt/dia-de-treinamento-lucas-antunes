﻿<%@ Page Title="Pesquisa Tema" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PesquisaTema.aspx.cs" Inherits="WebApp.PesquisaTema" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2>Insira o Id do Tema Desejado</h2>
        <hr />
        <br />
        <asp:TextBox id="IdTema" class="custom-text-box" runat="server" Height="23px" BorderStyle="Inset"></asp:TextBox>
        <asp:Button class="custom-btn" runat="server" Text="Pesquisar" OnClick="PesquisarBtn"/>
     </div>
</asp:Content>