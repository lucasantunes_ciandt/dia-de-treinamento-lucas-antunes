﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Service;
using Infrastructure;

namespace WebApp
{
    public partial class LivrosTema : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int idTema = Convert.ToInt32(Session["Tema"]);
            var livros = new LivrariaService().ObterLivrosPorTema(idTema);
            string tema = new LivrariaService().ObterTemaPorId(idTema).Descricao;

            HeaderLivros.InnerText += " de " + tema;

            if (livros != null && livros.Count > 0)
            {
                this.GridLivros.DataSource = livros;
                this.GridLivros.DataBind();
            }
        }

        protected void VoltarBtn(object sender, EventArgs e)
        {
            Response.Redirect("PesquisaTema.aspx");
        }
    }
}