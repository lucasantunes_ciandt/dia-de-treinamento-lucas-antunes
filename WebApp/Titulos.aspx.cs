﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Titulos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var livros = new LivrariaService().ObterLivros();

                if (livros != null && livros.Count > 0)
                {
                    this.GridLivros.DataSource = livros;
                    this.GridLivros.DataBind();
                }
            }
        }
    }
}