﻿<%@ Page Title="Títulos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Titulos.aspx.cs" Inherits="WebApp.Titulos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="middle">
        <h2>Títulos</h2>
        <hr />
        <br />
        <div>
            <asp:GridView id="GridLivros" runat="server" AutoGenerateColumns="false" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="Titulo" HeaderText="Título"/>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
