﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class PesquisaAutor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PesquisarBtn(object sender, EventArgs e)
        {
            Session["Autor"] = IdAutor.Text;
            Response.Redirect("LivrosAutor.aspx");
        }
    }
}