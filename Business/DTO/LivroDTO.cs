﻿using System;

namespace Business.DTO
{
    public class LivroDTO
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public double Valor { get; set; }
        public int IdAutor { get; set; }
        public int IdTema { get; set; }
    }
}
