﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.DTO
{
    public class AutorLivroDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
