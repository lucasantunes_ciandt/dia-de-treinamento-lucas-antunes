﻿using Business.DTO;
using Infrastructure;
using Model.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class LivrariaBusiness
    {
        public List<LivroDTO> ObterLivros()
        {
            LivrariaRepository service = new LivrariaRepository();
            return service.ObterLivros().Select(p =>
            new LivroDTO
            {
                Id = p.Id,
                Titulo = p.Titulo,
                Valor = p.Valor,
                IdAutor = p.IdAutor,
                IdTema = p.IdTema
            }).ToList();
        }
        public List<AutorLivroDTO> ObterAutores()
        {
            LivrariaRepository service = new LivrariaRepository();
            return service.ObterAutores().Select(p =>
            new AutorLivroDTO
            {
                Id = p.Id,
                Nome = p.Nome
            }).ToList();
        }

        public List<TemaLivroDTO> ObterTemas()
        {
            LivrariaRepository service = new LivrariaRepository();
            return service.ObterTemas().Select(p =>
            new TemaLivroDTO
            {
                Id = p.Id,
                Descricao = p.Descricao
            }).ToList();
        }

        public List<LivroDTO> ObterLivrosPorAutor(int id)
        {
            return ObterLivros().Where(p => p.IdAutor == id).ToList();
        }

        public List<LivroDTO> ObterLivrosPorTema(int id)
        {
            var ListaLivros = ObterLivros().Where(p => p.IdTema == id).ToList();
            if (ListaLivros.Any())
                return ListaLivros;
            else
                throw new EmptyListException();
        }

        public AutorLivroDTO ObterAutorPorId(int id)
        {
            LivrariaRepository service = new LivrariaRepository();
            return service.ObterAutores().Where(p => p.Id == id).Select(p =>
            new AutorLivroDTO
            {
                Id = p.Id,
                Nome = p.Nome
            }).FirstOrDefault();
        }

        public TemaLivroDTO ObterTemaPorId(int id)
        {
            LivrariaRepository service = new LivrariaRepository();
            return service.ObterTemas().Where(p => p.Id == id).Select(p =>
            new TemaLivroDTO
            {
                Id = p.Id,
                Descricao = p.Descricao
            }).FirstOrDefault();
        }
    }
}
