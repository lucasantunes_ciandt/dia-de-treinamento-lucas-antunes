﻿using Model.Model;
using System.Collections.Generic;

namespace Model.Repository
{
    public class LivrariaRepository
    {
        public List<LivroModel> ObterLivros()
        {
            return new List<LivroModel>
            {
                new LivroModel
                {
                    Id = 1,
                    Titulo = "Poeira em Alto Mar",
                    Valor = 100.40,
                    IdAutor = 1,
                    IdTema = 1
                },
                new LivroModel
                {
                    Id = 2,
                    Titulo = "As Tranças do Rei Careca",
                    Valor = 60.40,
                    IdAutor = 2,
                    IdTema = 2
                },
                new LivroModel
                {
                    Id = 3,
                    Titulo = "Helicóptero doido",
                    Valor = 40.40,
                    IdAutor = 3,
                    IdTema = 3
                },
                new LivroModel
                {
                    Id = 4,
                    Titulo = "Poeira no Rio",
                    Valor = 50.48,
                    IdAutor = 4,
                    IdTema = 1
                },
                new LivroModel
                {
                    Id = 5,
                    Titulo = "Bla bla Pombo",
                    Valor = 90.40,
                    IdAutor = 5,
                    IdTema = 3
                }
            };
        }

        public List<TemaLivroModel> ObterTemas()
        {
            return new List<TemaLivroModel>
            {
                new TemaLivroModel
                {
                    Id = 1,
                    Descricao = "Suspense"
                },
                new TemaLivroModel
                {
                    Id = 2,
                    Descricao = "Romance"
                },
                new TemaLivroModel
                {
                    Id = 3,
                    Descricao = "Ação"
                }
            };
        }

        public List<AutorLivroModel> ObterAutores()
        {
            return new List<AutorLivroModel> {
                new AutorLivroModel
                {
                    Id = 1,
                    Nome = "Felipe"
                },
                new AutorLivroModel
                {
                    Id = 2,
                    Nome = "Amanda"
                },
                new AutorLivroModel
                {
                    Id = 3,
                    Nome = "Ailton"
                },
                new AutorLivroModel
                {
                    Id = 4,
                    Nome = "Bitar"
                },
                new AutorLivroModel
                {
                    Id = 5,
                    Nome = "Vinícius"
                }
            };
        }
    }
}
