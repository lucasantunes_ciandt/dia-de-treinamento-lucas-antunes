﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Model
{
    public class AutorLivroModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
