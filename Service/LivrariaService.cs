﻿using Business;
using Business.DTO;
using System.Collections.Generic;

namespace Service
{
    public class LivrariaService
    {
        public List<LivroDTO> ObterLivros()
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterLivros();
        }

        public List<AutorLivroDTO> ObterAutores()
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterAutores();
        }

        public List<TemaLivroDTO> ObterTemas()
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterTemas();
        }

        public List<LivroDTO> ObterLivrosPorAutor(int id)
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterLivrosPorAutor(id);
        }

        public List<LivroDTO> ObterLivrosPorTema(int id)
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterLivrosPorTema(id);
        }

        public AutorLivroDTO ObterAutorPorId(int id)
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterAutorPorId(id);
        }

        public TemaLivroDTO ObterTemaPorId(int id)
        {
            LivrariaBusiness business = new LivrariaBusiness();
            return business.ObterTemaPorId(id);
        }
    }
}
