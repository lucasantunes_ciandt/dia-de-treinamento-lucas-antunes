﻿using Exercicio;
using Infrastructure;
using Service;
using System;

namespace ExercicioConsole
{
    class Program : Master
    {
        static void Main(string[] args)
        {
            var service = new LivrariaService();
            string valor;
            do
            {
                RetornaMenu();
                valor = Console.ReadLine();

                if (valor == "1")
                {
                    Console.Clear();
                    Console.WriteLine("****************LIVROS****************");
                    Console.WriteLine("");
                    foreach (var item in service.ObterLivros())
                    {
                        var Autor = service.ObterAutorPorId(item.IdAutor);
                        var tema = service.ObterTemaPorId(item.IdTema);
                        Console.WriteLine("Autor: " + Autor.Nome);
                        Console.WriteLine("Titulo: " + item.Titulo);
                        Console.WriteLine("Tema: " + tema.Descricao);
                        Console.WriteLine("Valor: " + item.Valor);
                        Console.WriteLine("");
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();

                }

                if (valor == "2")
                {
                    Console.Clear();
                    Console.WriteLine("****************AUTORES****************");
                    Console.WriteLine("");
                    foreach (var autor in service.ObterAutores())
                    {
                        Console.WriteLine(autor.Nome);
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();
                }

                if (valor == "3")
                {
                    Console.Clear();
                    Console.WriteLine("****************TÍTULOS****************");
                    Console.WriteLine("");
                    foreach (var item in service.ObterLivros())
                    {
                        Console.WriteLine(item.Titulo);
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();
                }

                if (valor == "4")
                {
                    Console.Clear();
                    Console.WriteLine("****************TEMAS****************");
                    Console.WriteLine("");
                    foreach (var tema in service.ObterTemas())
                    {
                        Console.WriteLine(tema.Descricao);
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();
                }

                if (valor == "5")
                {
                    Console.Clear();
                    Console.WriteLine("****************LIVROS****************");
                    Console.WriteLine("");
                    Console.Write("Informe o ID do tema: ");
                    string temaId = Console.ReadLine();
                    Console.WriteLine("");
                    try
                    {
                        foreach (var item in service.ObterLivrosPorTema(Convert.ToInt32(temaId)))
                        {
                            var autor = service.ObterAutorPorId(item.IdAutor);
                            var tema = service.ObterTemaPorId(item.IdTema);
                            Console.WriteLine("Autor: " + autor.Nome);
                            Console.WriteLine("Titulo: " + item.Titulo);
                            Console.WriteLine("Tema: " + tema.Descricao);
                            Console.WriteLine("Valor: " + item.Valor);
                            Console.WriteLine("");
                        }
                    }
                    catch (EmptyListException)
                    {
                        Console.WriteLine("Não existe um tema com o id " + temaId + "!");
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("O ID deve ser um número!");
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Ops! Ocorreu um erro inesperado. Favor tente novamente.");
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();
                }

                if (valor == "6")
                {
                    Console.Clear();
                    Console.WriteLine("****************LIVROS****************");
                    Console.WriteLine("");
                    Console.Write("Informe o ID do autor: ");
                    string autorId = Console.ReadLine();
                    Console.WriteLine("");
                    try
                    {
                        foreach (var item in service.ObterLivrosPorAutor(Convert.ToInt32(autorId)))
                        {
                            var tema = service.ObterTemaPorId(item.IdTema);
                            Console.WriteLine("Titulo: " + item.Titulo);
                            Console.WriteLine("Tema: " + tema.Descricao);
                            Console.WriteLine("Valor: " + item.Valor);
                            Console.WriteLine("");
                        }
                    }
                    catch (EmptyListException)
                    {
                        Console.WriteLine("Não existe um autor com o id + " + autorId + "!");
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("O ID deve ser um número!");
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Ops! Ocorreu um erro inesperado. Favor tente novamente.");
                    }

                    Console.WriteLine("");
                    Console.WriteLine("Aperte enter para continuar!");
                    Console.ReadLine();
                }
            } while (valor != "0");

            Console.WriteLine("*****************************");

        }
    }
}
